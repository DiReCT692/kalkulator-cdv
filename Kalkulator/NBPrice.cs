﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class NBPrice
    {
        public void NetAmount()
        {
            Console.WriteLine("VAT:");
            int vat = Convert.ToInt32(Console.ReadLine());
            while (vat > 100 || vat < 0)
            {
                Console.WriteLine("VAT:");
                vat = Convert.ToInt32(Console.ReadLine());

            }
            int vatAmount = 100 - vat;
            Console.WriteLine("Set gross amount: ");
            double amount = Convert.ToInt32(Console.ReadLine());
            double result = amount * vatAmount / 100;
            Console.WriteLine("Net amount from {0} is {1}", amount, result);
        }

        public void GrossAmount()
        {
            Console.WriteLine("VAT:");
            int vat = Convert.ToInt32(Console.ReadLine());
            while (vat > 100 || vat < 0)
            {
                Console.WriteLine("VAT:");
                vat = Convert.ToInt32(Console.ReadLine());

            }
            int vatAmount = 100 + vat;
            Console.WriteLine("Set net amount: ");
            double amount = Convert.ToInt32(Console.ReadLine());
            double result = amount * vatAmount / 100;
            Console.WriteLine("Gross amount from {0} is {1}", amount, result);
        }
    }
}
