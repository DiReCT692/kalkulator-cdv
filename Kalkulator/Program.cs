﻿using System;

namespace Kalkulator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welocome in calculator!\n1.Trigonometric\n2.Pi\n3...\n4...");
            Console.WriteLine("Enter what you want to calculate:");
            int function = Convert.ToInt32(Console.ReadLine());
            switch (function)
            {
                case 1:
                    Trigonometric countTrigonometric = new Trigonometric();
                    countTrigonometric.Trigon();
                    break;
                case 2:
                    Pi pi = new Pi();
                    pi.Pi2();
                    break;
                case 3:
                    logarithm log = new logarithm();
                    log.Log();
                    break;
                case 4:
                    Square sqr = new Square();
                    sqr.Sqr();
                    break;
                case 5:
                    Power pow = new Power();
                    pow.Pow();
                    break;
                case 6:
                    NBPrice net = new NBPrice();
                    net.NetAmount();
                    break;
                case 7:
                    NBPrice gross = new NBPrice();
                    gross.GrossAmount();
                    break;
                case 8:
                    Zus contributionEmployee = new Zus();
                    contributionEmployee.ContributionEmployee();
                    break;
                case 9:
                    Zus contributionEmployer = new Zus();
                    contributionEmployer.ContributionEmployer();
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }
    }
}
