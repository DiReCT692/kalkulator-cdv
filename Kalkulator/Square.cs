﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class Square
    {
        public void Sqr()
        {
            Console.Write("Set number: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double sqrt = Math.Sqrt(x);
            Console.WriteLine("Square ({0}) is {1}", x, sqrt);
        }
    }
}
