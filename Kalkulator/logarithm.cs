﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class logarithm
    {
        public void Log()
        {
            Console.Write("Set number: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double log = Math.Log10(x);
            Console.WriteLine("Log({0}) is {1}", x, log);
        }
    }
}
