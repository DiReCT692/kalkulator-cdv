﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class Pi
    {
        public void Pi2()
        {
            Console.Write("Set number: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double pi = Math.PI;
            double wynik = x * pi;
            Console.WriteLine("PI({0}) is {1}", x, wynik);
        }
    }
}
