﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class Trigonometric
    {
        public void Trigon()
        {
            Console.WriteLine("1.Sinus\n2.Cosinus\n3.Tangens\n4.Cotangens");
            Console.WriteLine("Enter number of function: ");
            int number = Convert.ToInt32(Console.ReadLine());
            switch (number)
            {
                case 1:
                    Sinus();
                    break;
                case 2:
                    Cosinus();
                    break;
                case 3:
                    Tangens();
                    break;
                case 4:
                    Cotangens();
                    break;
                default:
                    Console.WriteLine("Nieprawidłowa wartość");
                    break;
            }
        }
        public void Sinus()
        {
            Console.Write("Set number in degrees: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double radian = x * (Math.PI/180);
            double sinus = Math.Sin(radian);
            Console.WriteLine("Sinus {0}° is {1}", x, sinus);
        }

        public void Cosinus()
        {
            Console.Write("Set number in degrees: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double radian = x * (Math.PI/180);
            double cosinus = Math.Cos(radian);
            Console.WriteLine("Cosinus {0}° is {1}", x, cosinus);
        }
        public void Tangens()
        {
            Console.Write("Set number in degrees: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double radian = x * (Math.PI/180);
            double tangens = Math.Tan(radian);
            Console.WriteLine("Tangens {0}° is {1}", x, tangens);
        }
        public void Cotangens()
        {
            Console.Write("Set number in degrees: ");
            int x = Convert.ToInt32(Console.ReadLine());
            double radian = x * (Math.PI / 180);
            double tangens = 1/Math.Tan(radian);
            Console.WriteLine("Cotangens {0}° is {1}", x, tangens);
        }
    }
}
