﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalkulator
{
    class Zus
    {
        public void ContributionEmployee()
        {
            double health = 0.09;
            double disabilityPension = 0.015;
            double pension = 0.0976;
            double sickness = 0.0245;
            Console.WriteLine("Set gross salary basis");
            double salary = Convert.ToInt32(Console.ReadLine());
            double social = (salary * pension) + (salary * sickness) + (salary * disabilityPension);
            double healthContribution = (salary - social) * health;
            Console.WriteLine("Net salary for you: {0}", salary - (social + healthContribution));
            Console.WriteLine("Sum of contribution: {0}", social + healthContribution);
        }

        public void ContributionEmployer()
        {
            double pension = 0.0976;
            double disabilityPension = 0.065;
            double accident = 0.0167;
            double FP = 0.0245;
            double FGSP = 0.001;
            Console.WriteLine("Set gross salary basis");
            double salary = Convert.ToInt32(Console.ReadLine());
            double result = (salary * pension) + (salary * disabilityPension) + (salary * accident) + (salary * FP) + (salary * FGSP);
            Console.WriteLine("Employer must pay contributions for the employee in the amount of: {0}", result);
        }
    }
}
